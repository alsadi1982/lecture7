package sbp.http;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
