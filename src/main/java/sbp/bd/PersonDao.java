package sbp.bd;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonDao {

    private static final String LOCAL_URL = "jdbc:sqlite:C:\\Users\\sav19\\.m2\\repository\\ru\\edu\\lecture7\\sqlite_person.db";

    /**
     * Method PersonDao#createPerson(User user) insert new user into table Users
     * @param user - user for insertion
     * @return true - if user inserted, false - if not
     */
    public boolean createPerson(User user){

        final String createPersonRequest = "insert into Users (name, age, city) values(?,?,?)";
        int affectedRows = 0;

        synchronized (PersonDao.class) {
            try (Connection connection = DriverManager.getConnection(LOCAL_URL);
                 PreparedStatement preparedStatement = connection.prepareStatement(createPersonRequest)) {

                preparedStatement.setString(1, user.getName());
                preparedStatement.setInt(2, user.getAge());
                preparedStatement.setString(3, user.getCity());
                affectedRows = preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.getSQLState());
            }
        }

        return affectedRows > 0;
    }

    /**
     * Method PersonDao#getAllPersons() returned collection of users
     * @return List of users
     */
    public List<User> getAllPersons(){

        final String getAllPersonsQuery = "Select * from Users";
        List<User> list = new ArrayList<>();

        synchronized (PersonDao.class) {
            try (Connection connection = DriverManager.getConnection(LOCAL_URL);
                 Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(getAllPersonsQuery)) {

                while (resultSet.next()) {
                    User user = new User(resultSet.getString("name"),
                            resultSet.getString("city"),
                            resultSet.getInt("age"));
                    list.add(user);
                }
            } catch (SQLException ex) {
                ex.getStackTrace();
            }

        }
        return list;
    }

    /**
     * Method PersonDao#getPersonById(int id) get user by ID
     * @param id - unique user ID
     * @return user with this id
     * @throws  IllegalArgumentException if user doesn't exist
     */
    public User getPersonById(int id){

        final String getPersonByNameQuery = "Select * from Users where id = ?";
        User user = null;

        synchronized (PersonDao.class) {
            try (Connection connection = DriverManager.getConnection(LOCAL_URL);
                 PreparedStatement preparedStatement = connection.prepareStatement(getPersonByNameQuery)) {

                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                user = new User(resultSet.getString("name"),
                        resultSet.getString("city"),
                        resultSet.getInt("age"));

                resultSet.close();

            } catch (SQLException ex) {
                ex.getStackTrace();
            }
        }

        if (user == null){
            throw new IllegalArgumentException("User with id = " + id + " doesn't exist!");
        }

        return user;
    }

    /**
     * Method PersonDao#deletePersonByName(String name) delete user from table Users by name
     * @param name - user's name which we want to delete
     * @return true - if we deleted user
     * @throws IllegalArgumentException if user not deleted
     */
    public boolean deletePersonByName(String name){

        final String deletePersonQuery = "delete from Users where name = ?";
        int affectedRows = 0;

        synchronized (PersonDao.class) {
            try (Connection connection = DriverManager.getConnection(LOCAL_URL);
                 PreparedStatement preparedStatement = connection.prepareStatement(deletePersonQuery)) {

                preparedStatement.setString(1, name);
                affectedRows = preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        if (affectedRows == 0){
            throw new IllegalArgumentException("User not found!");
        }

        return affectedRows > 0;
    }

    /**
     * Method PersonDao#updatePersonById(int id, String name, String city, int age) update user with given ID
     * @param id  which we want to update
     * @param name - new user's name
     * @param city - new user's city
     * @param age - new user's age
     * @return true -if we updated user, false - if not
     */
    public boolean updatePersonById(int id, String name, String city, int age){

        getPersonById(id);

        final String updatePersonByIdQuery = "update Users set name = ?, city = ?, age = ? where id = ?";
        int affectedRows = 0;

        synchronized (PersonDao.class) {
            try (Connection connection = DriverManager.getConnection(LOCAL_URL);
                 PreparedStatement preparedStatement = connection.prepareStatement(updatePersonByIdQuery)) {

                preparedStatement.setString(1, name);
                preparedStatement.setString(2, city);
                preparedStatement.setInt(3, age);
                preparedStatement.setInt(4, id);
                affectedRows = preparedStatement.executeUpdate();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return affectedRows > 0;

    }

}
