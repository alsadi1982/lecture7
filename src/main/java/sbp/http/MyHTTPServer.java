package sbp.http;

import com.sun.net.httpserver.HttpServer;
import sbp.bd.PersonDao;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class MyHTTPServer {
    private static final String IP = "localhost";
    private static final int PORT = 8000;

    public void startMyServer() throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(IP, PORT), 0);
        httpServer.createContext("/", new BDHandler(new PersonDao()));
        httpServer.createContext("/access_info", new GetUserHandler(new PersonDao()));

        httpServer.setExecutor(Executors.newFixedThreadPool(10));
        httpServer.start();

        System.out.println("My server started!");
    }


}
