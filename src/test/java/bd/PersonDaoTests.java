package bd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.bd.PersonDao;
import sbp.bd.User;

import java.util.ArrayList;
import java.util.List;

public class PersonDaoTests {

    /**
     * Checking the successful execution PersonDao#createPerson(User user)
     */
    @Test
    public void createPerson_Test(){

        PersonDao personDao = new PersonDao();
        Assertions.assertTrue(personDao.createPerson(new User("Oleg", "Nizhny Novgorod", 20)));

    }

    /**
     * Checking the successful execution PersonDao#getAllPersons()
     */
    @Test
    public void getAllPersons_Test(){

        PersonDao personDao = new PersonDao();
        List<User> expectedList = new ArrayList<>();
        expectedList.add(new User("Volodya", "Eburg", 20));
        expectedList.add(new User("Pavel", "Novgorod", 26));
        expectedList.add(new User("Sergey", "Voronej", 30));
        expectedList.add(new User("Oleg", "Nizhny Novgorod", 20));
        List<User> actualList = personDao.getAllPersons();

        Assertions.assertArrayEquals(expectedList.toArray(), actualList.toArray());

    }

    /**
     * Checking the successful execution PersonDao#getPersonById(int id)
     */
    @Test
    public void getPersonById_Successful_Test(){

        PersonDao personDao = new PersonDao();
        User expectedUser = new User("Pavel", "Novgorod", 26);
        User actualUser = personDao.getPersonById(2);

        Assertions.assertEquals(expectedUser, actualUser);

    }

    /**
     * Checking the unsuccessful execution PersonDao#getPersonById(int id)
     * Checking scenario where this ID doesn't exist
     */
    @Test
    public void getPersonById_Fail_Test(){

        PersonDao personDao = new PersonDao();
        Assertions.assertThrows(IllegalArgumentException.class, () -> personDao.getPersonById(6));
    }

    /**
     * Checking the successful execution PersonDao#deletePersonByName(String name)
     */
    @Test
    public void deletePersonByName_Successful_Test(){

        PersonDao personDao = new PersonDao();

        Assertions.assertTrue(personDao.deletePersonByName("Oleg"));

    }

    /**
     * Checking the unsuccessful execution PersonDao#deletePersonByName(String name)
     * Checking scenario where user with this name doesn't exist
     */
    @Test
    public void deletePersonByName_Fail_Test(){

        PersonDao personDao = new PersonDao();
        Assertions.assertThrows(IllegalArgumentException.class, () -> personDao.deletePersonByName("Kirill"));
    }

    /**
     * Checking the unsuccessful execution PersonDao#updatePersonById(int id, String name, String city, int age)
     */
    @Test
    public void updatePersonById_Successful_Test(){

        PersonDao personDao = new PersonDao();
        Assertions.assertTrue(personDao.updatePersonById(7, "Olga", "Krasnodar", 22));
    }

    /**
     * Checking the unsuccessful execution PersonDao#updatePersonById(int id, String name, String city, int age)
     * Checking scenario where this ID doesn't exist
     */
    @Test
    public void updatePersonById_Fail_Test(){

        PersonDao personDao = new PersonDao();
        Assertions.assertThrows(IllegalArgumentException.class, () -> personDao.updatePersonById(10, "Olga", "Krasnodar", 22));
    }



}
