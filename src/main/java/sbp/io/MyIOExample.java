package sbp.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MyIOExample
{
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName)
    {

        Path path = Paths.get(fileName);

        if (Files.exists(path)){
            if (Files.isRegularFile(path)){
                System.out.println(path.getFileName() + " is FILE!");
            }else if (Files.isDirectory(path)){
                System.out.println(path.getFileName() + " is DIRECTORY!");
            }

            Path absolutePath = path.toAbsolutePath();
            Path relativePath = absolutePath.subpath(absolutePath.getNameCount() - 2, absolutePath.getNameCount());
            System.out.println("Absolute path: " + absolutePath);
            if(path.isAbsolute()) {
                System.out.println("Relative path: " + relativePath);
            }else{
                System.out.println("Relative path: " + path);
            }

            if (Files.isRegularFile(path)){
                try {
                    System.out.println("File size: " + Files.size(path));
                    System.out.println("Last modified time: " + Files.getLastModifiedTime(path));
                }catch(IOException ex){
                    System.out.println(ex.getMessage());
                    ex.getStackTrace();
                }
            }

            return false;
        }

        System.out.println("File has been created!");
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException
    {

        Files.copy(Paths.get(sourceFileName), Paths.get(destinationFileName));
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        try (BufferedReader reader = Files.newBufferedReader(sourcePath, StandardCharsets.US_ASCII);
             BufferedWriter writer = Files.newBufferedWriter(destinationPath, StandardCharsets.US_ASCII)){
                String line = null;
                while ((line = reader.readLine()) != null){
                    writer.write(line + "\n");
                }
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        try (InputStream input = Files.newInputStream(sourcePath);
             OutputStream output = Files.newOutputStream(destinationPath)){

            int data;
            while ((data = input.read()) != -1){
                output.write((char)data);
            }

        }catch (IOException ex){
            ex.printStackTrace();
        }
        return true;
    }

    public boolean copyFileWithReadAllLines(String sourceFileName, String destinationFileName)
    {
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        try(BufferedWriter writer = Files.newBufferedWriter(destinationPath)) {
            List<String> str = Files.readAllLines(sourcePath);
            str.forEach(s ->{
                try{
                    writer.write(s + "\n");
                }catch(IOException e){
                    e.printStackTrace();
                }});
        }catch (IOException ex){
            ex.printStackTrace();
        }

        return true;
    }
}
