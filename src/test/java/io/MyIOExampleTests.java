package io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MyIOExampleTests {

    /**
     * Checking the successful execution MyIOExample#workWithFile(String fileName)
     * Checking situation when the file is already exist
     */
    @Test
    public void workWithFile_Exist_Test(){

        MyIOExample myIOExample = new MyIOExample();

        try {
            Path path = Files.createTempFile("input", ".txt");
            boolean actual = myIOExample.workWithFile(path.toString());
            Assertions.assertFalse(actual);
        }catch(IOException ex){
            ex.printStackTrace();
        }

    }

    /**
     * Checking the successful execution MyIOExample#workWithFile(String fileName)
     * Checking situation when the file is not exist
     */
    @Test
    public void workWithFile_NotExist_Test(){

        MyIOExample myIOExample = new MyIOExample();

        boolean actual = myIOExample.workWithFile("file.txt");
        Assertions.assertTrue(actual);

    }

    /**
     * Checking the successful execution MyIOExample#copyFile(String sourceFileName, String destinationFileName)
     */
    @Test
    public void copyFile_Test() throws IOException{

        MyIOExample myIOExample = new MyIOExample();

        Path path = Files.createTempFile("input", ".txt");
        boolean actual = myIOExample.copyFile(path.toString(), "src/main/java/myFile_copy1");
        Assertions.assertTrue(actual);

    }

    /**
     * Checking the unsuccessful execution MyIOExample#copyFile(String sourceFileName, String destinationFileName)
     * Checking situation where source file doesn't exist
     */
    @Test
    public void copyFile_Unsuccessful_Test(){

        MyIOExample myIOExample = new MyIOExample();

        Assertions.assertThrows(IOException.class, () -> myIOExample.copyFile("text\\test", "src/main/java/myFile_copy2"));

    }

    /**
     * Checking the successful execution MyIOExample#copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
     */
    @Test
    public void copyFileWithReaderAndWriter_Test() throws IOException{

        MyIOExample myIOExample = new MyIOExample();

        Path path = Files.createTempFile("input", ".txt");
        boolean actual = myIOExample.copyFileWithReaderAndWriter(path.toString(), "src/main/java/myFile_copy3");
        Assertions.assertTrue(actual);

    }

    /**
     * Checking the successful execution MyIOExample#copyBufferedFile(String sourceFileName, String destinationFileName)
     */
    @Test
    public void copyBufferedFile_Test() throws IOException{

        MyIOExample myIOExample = new MyIOExample();

        Path path = Files.createTempFile("input", ".txt");
        boolean actual = myIOExample.copyBufferedFile(path.toString(), "src/main/java/myFile_copy4");
        Assertions.assertTrue(actual);

    }

    /**
     * Checking the successful execution MyIOExample#copyFileWithReadAllLines(String sourceFileName, String destinationFileName)
     */
    @Test
    public void copyFileWithReadAllLines_Test() throws IOException{

        MyIOExample myIOExample = new MyIOExample();

        Path path = Files.createTempFile("input", ".txt");
        boolean actual = myIOExample.copyFileWithReadAllLines(path.toString(), "src/main/java/myFile_copy5");
        Assertions.assertTrue(actual);

    }
}
