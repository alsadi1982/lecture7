package httptests;

import com.sun.net.httpserver.HttpExchange;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.bd.PersonDao;
import sbp.http.GetUserHandler;
import sbp.http.MyHTTPServer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class GetUserHandlerTests {

    /**
     * Checking the successful execution GetUserHandler#getUserExecute(HttpExchange exchange)
     */
    @Test
    public void getUserExecute_Success_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("GET");

        InputStream inputStream = new ByteArrayInputStream("{\"id\": 27}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        GetUserHandler bd = new GetUserHandler(new PersonDao());
        String expected = "User with id = 27 name = den, city = Kirov, age = 20, access =  ORDINARY";
        String actual = bd.getUserExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the unsuccessful execution GetUserHandler#getUserExecute(HttpExchange exchange)
     */
    @Test
    public void getUserExecute_Fail_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("GET");

        InputStream inputStream = new ByteArrayInputStream("{\"id\": 100}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        GetUserHandler bd = new GetUserHandler(new PersonDao());
        String expected = "User with id = 100 doesn't exist!";
        String actual = bd.getUserExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }
}
