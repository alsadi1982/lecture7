package httptests;

import com.sun.net.httpserver.HttpExchange;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.bd.PersonDao;
import sbp.http.BDHandler;
import sbp.http.MyHTTPServer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BDHandlerTests {

    /**
     * Checking the successful execution BDHandler#postExecute(HttpExchange exchange)
     */
    @Test
    public void postExecute_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("POST");

        InputStream inputStream = new ByteArrayInputStream("{\"name\":\"den\", \"city\":\"Kirov\", \"age\": 20}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        BDHandler bd = new BDHandler(new PersonDao());
        String expected = "User den successful created";
        String actual = bd.postExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the successful execution BDHandler#deleteExecute(HttpExchange exchange)
     */
    @Test
    public void deleteExecute_Success_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("DELETE");

        InputStream inputStream = new ByteArrayInputStream("{\"name\":\"den\"}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        BDHandler bd = new BDHandler(new PersonDao());
        String expected = "User den successful deleted";
        String actual = bd.deleteExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the unsuccessful execution BDHandler#deleteExecute(HttpExchange exchange)
     */
    @Test
    public void deleteExecute_Fail_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("DELETE");

        InputStream inputStream = new ByteArrayInputStream("{\"name\":\"den\"}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        BDHandler bd = new BDHandler(new PersonDao());
        String expected = "User den not found";
        String actual = bd.deleteExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the successful execution BDHandler#putExecute(HttpExchange exchange)
     */
    @Test
    public void putExecute_Success_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("PUT");

        InputStream inputStream = new ByteArrayInputStream("{\"name\":\"den\", \"city\":\"Kirov\", \"age\": 20, \"id\": 27}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        BDHandler bd = new BDHandler(new PersonDao());
        String expected = "User with id = 27 successful updated! New data: name = den, city = Kirov, age = 20";
        String actual = bd.putExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }

    /**
     * Checking the unsuccessful execution BDHandler#putExecute(HttpExchange exchange)
     */
    @Test
    public void putExecute_Fail_Test(){
        MyHTTPServer myHTTPServer = new MyHTTPServer();
        try {
            myHTTPServer.startMyServer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpExchange exchangeMock = Mockito.mock(HttpExchange.class);
        Mockito.when(exchangeMock.getRequestMethod()).thenReturn("PUT");

        InputStream inputStream = new ByteArrayInputStream("{\"name\":\"den\", \"city\":\"Kirov\", \"age\": 20, \"id\": 100}".getBytes());
        Mockito.when(exchangeMock.getRequestBody()).thenReturn(inputStream);

        BDHandler bd = new BDHandler(new PersonDao());
        String expected = "User with id = 100 doesn't exist!";
        String actual = bd.putExecute(exchangeMock);
        Assertions.assertEquals(expected, actual);

    }
}
