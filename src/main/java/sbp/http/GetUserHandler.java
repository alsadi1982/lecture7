package sbp.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;
import sbp.bd.PersonDao;
import sbp.bd.User;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class GetUserHandler implements HttpHandler {

    private final PersonDao dao;
    private volatile Map<Integer, String> cashe = new HashMap<>();
    private volatile String message = "";

    public GetUserHandler(PersonDao dao) {
        this.dao = dao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        StringBuilder htmlBuilder = new StringBuilder();
        Thread thread = new Thread(() -> message = this.getUserExecute(exchange));
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        OutputStream outputStream1 = exchange.getResponseBody();

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1 align=\"center\">Users access</h1>")
                .append("<table width=\"400\" bgcolor=\"#F08080\" border=\"1\" align=\"center\" cellspacing=\"5\" cellpadding\"10\">" +
                "<tr>" +
                "<th>ID</th>" +
                "<th>Access</th>" +
                "</tr>");
        for (Map.Entry<Integer, String> entry: cashe.entrySet()){
            htmlBuilder.append("<tr><td>")
                    .append(entry.getKey())
                    .append("</td><td>")
                    .append(entry.getValue())
                    .append("</td></tr>");
        }
       htmlBuilder.append("</table>")
                .append("<h1 align=\"center\">" + message + "</h1>")
                .append("</body>")
                .append("</html>");

        String htmlResponse = htmlBuilder.toString();
        exchange.sendResponseHeaders(202, htmlResponse.length());
        outputStream1.write(htmlResponse.getBytes(StandardCharsets.UTF_8));
        outputStream1.flush();
        outputStream1.close();

    }

    /**
     * Method GetUserHandler#getUserExecute() implements GET request by ID and displays user info
     * @param exchange
     * @return string with result of request
     */
    public String getUserExecute(HttpExchange exchange){

        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
        String requestBody = reader.lines().collect(Collectors.joining());
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject(requestBody);
        int id = jsonObject.getInt("id");

            User user;
        try {
            user = dao.getPersonById(id);
            if (!cashe.containsKey(id)){
                if(id%2 == 0) {
                    cashe.put(id, "VIP");
                }else {
                    cashe.put(id, "ORDINARY");
                }
            }
        }catch(IllegalArgumentException ex){
            return "User with id = " + id + " doesn't exist!";
        }

        return "User with id = " + id
                + " name = " + user.getName()
                + ", city = " + user.getCity()
                + ", age = " + user.getAge()
                + ", access =  " + cashe.get(id);
    }
}
