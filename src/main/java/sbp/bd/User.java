package sbp.bd;

import java.time.LocalDate;
import java.util.Objects;

public class User {

    private String name;
    private String city;
    private int age;

    public User(String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", city=" + city +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (this == other) return true;
        if (!this.getClass().equals(other.getClass())) return false;
        User user = (User) other;
        return this.name.equalsIgnoreCase(user.name) && this.city.equalsIgnoreCase(user.city) && this.age == user.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCity(), getAge());
    }
}
