package sbp.iterator;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {

    private T[][] array;
    private int rowIndex = 0;
    private int colIndex = 0;

    public ArrayIterator(T[][] array) {
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return colIndex < array.length;
    }

    @Override
    public T next() {
        T result = array[colIndex][rowIndex++];
        if (!(rowIndex < array[colIndex].length)){
            colIndex++;
            rowIndex = 0;
        }

        return result;
    }
}
