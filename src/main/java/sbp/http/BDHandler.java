package sbp.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;
import sbp.bd.PersonDao;
import sbp.bd.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class BDHandler implements HttpHandler {

    private final PersonDao dao;
    private volatile String valueFromDao = "empty";
    private volatile String message = "";

    public BDHandler(PersonDao dao) {
        this.dao = dao;
    }


    @Override
    public void handle(HttpExchange exchange) throws IOException {

        OutputStream outputStream = exchange.getResponseBody();

        if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {
            Thread thread1 = new Thread(() ->  valueFromDao = this.getExecute());
            thread1.start();
            try {
                thread1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
         else if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
            Thread thread2 = new Thread(() ->  message = this.postExecute(exchange));
            thread2.start();
            try {
                thread2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE")){
            Thread thread3 = new Thread(() ->  message = this.deleteExecute(exchange));
            thread3.start();
            try {
                thread3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else if(exchange.getRequestMethod().equalsIgnoreCase("PUT")){
            Thread thread4 = new Thread(() ->  message = this.putExecute(exchange));
            thread4.start();
            try {
                thread4.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1 align=\"center\">Database PERSONS</h1>")
                .append(valueFromDao)
                .append("<h1 align=\"center\">" + message + "</h1>")
                .append("</body>")
                .append("</html>");

        String htmlResponse = htmlBuilder.toString();
        exchange.sendResponseHeaders(200, htmlResponse.length());
        outputStream.write(htmlResponse.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Method BDHandler#getExecute() implements GET request and displays the table in the browser
     * @return string with html code of the table
     */
    private String getExecute(){
        StringBuilder htmlBuilder = new StringBuilder();

        htmlBuilder.append("<table width=\"600\" height=\"300\" bgcolor=\"#66CDAA\" border=\"1\" align=\"center\" cellspacing=\"5\" cellpadding\"10\">")
                .append("<tr>" +
                "<th>Name</th>" +
                "<th>City</th>" +
                "<th>Age</th>" +
                "</tr>");
        for (int i = 0; i < dao.getAllPersons().size(); i++) {
            htmlBuilder.append("<tr><td>")
                    .append(dao.getAllPersons().get(i).getName())
                    .append("</td><td>")
                    .append(dao.getAllPersons().get(i).getCity())
                    .append("</td><td>")
                    .append(dao.getAllPersons().get(i).getAge())
                    .append("</td></tr>");
        }
        htmlBuilder.append("</table>");
        return htmlBuilder.toString();
    }

    /**
     * Method BDHandler#postExecute(HttpExchange exchange) implements POST request and sends response with result
     * @param exchange
     * @return string with result of request
     */
    public String postExecute(HttpExchange exchange){

        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
        String requestBody = reader.lines().collect(Collectors.joining());
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject(requestBody);
        String name = jsonObject.getString("name");
        String city = jsonObject.getString("city");
        int age = jsonObject.getInt("age");

        User newUser = new User(name, city, age);

        boolean insertedOrNot = this.dao.createPerson(newUser);

        if (!insertedOrNot){
           return "User " + name + " not created";
        }

        return "User " + name + " successful created";

    }

    /**
     * Method BDHandler#deleteExecute(HttpExchange exchange) implements DELETE request, deletes user from DB and sends response with result
     * @param exchange
     * @return string with result of request
     */
    public String deleteExecute(HttpExchange exchange){
        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
        String requestBody = reader.lines().collect(Collectors.joining());
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject(requestBody);
        String name = jsonObject.getString("name");

        try {
            this.dao.deletePersonByName(name);
        }catch(IllegalArgumentException ex){
            return "User " + name + " not found";
        }

        return "User " + name + " successful deleted";
    }

    /**
     * Method BDHandler#putExecute(HttpExchange exchange) implements PUT request, updates user from DB by ID and sends response with result
     * @param exchange
     * @return string with result of request
     */
    public String putExecute(HttpExchange exchange){
        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
        String requestBody = reader.lines().collect(Collectors.joining());
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject(requestBody);
        String name = jsonObject.getString("name");
        String city = jsonObject.getString("city");
        int age = jsonObject.getInt("age");
        int id = jsonObject.getInt("id");

        try {
            this.dao.updatePersonById(id, name, city, age);
        }catch(IllegalArgumentException ex){
            return "User with id = " + id + " doesn't exist!";
        }

        return "User with id = " + id + " successful updated!" + " New data: name = " + name + ", city = " + city + ", age = " + age;
    }

}
