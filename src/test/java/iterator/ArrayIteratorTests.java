package iterator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.iterator.ArrayIterator;


public class ArrayIteratorTests {

    /**
     * Checking the successful execution ArrayIterator#hasNext()
     */
    @Test
    public void hasNext_Test() {
        Integer[][] twoDimArray = {{1,2},{4,5,6}};
        ArrayIterator arrayIterator = new ArrayIterator(twoDimArray);
        Assertions.assertTrue(arrayIterator.hasNext());

        arrayIterator = new ArrayIterator(twoDimArray);
        for(int i = 0; i < 5; i++){
            arrayIterator.next();
        }
        Assertions.assertFalse(arrayIterator.hasNext());

    }

    /**
     * Checking the successful execution ArrayIterator#next()
     */
    @Test
    public void next_Test() {
        String[][] twoDimArray = {{"one","two"},{"three","four", "five"}};
        ArrayIterator arrayIterator = new ArrayIterator(twoDimArray);
        String expected = "one";
        String actual = (String) arrayIterator.next();
        Assertions.assertEquals(expected, actual);

    }
}
